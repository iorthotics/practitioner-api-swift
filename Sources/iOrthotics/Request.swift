import Foundation

// Represents a request.
public class Request<ResponseDataType: Decodable> : RequestBase
{
	/**
		Adds a header to the request.
		- Parameter value: The value of the header.
		- Parameter forHTTPHeaderField: The HTTP header field name.
	*/
	func addValue(_ value: String, forHTTPHeaderField: String) -> Request<ResponseDataType>
	{
		request.addValue(value, forHTTPHeaderField: forHTTPHeaderField)
		return self
	}
	
	/**
		Executes the task and returns it.
		- Parameter completion: The closure to call when the response is received.
	*/
	func executeTask(completion: @escaping (Response<ResponseDataType>) -> Void)
	{
		let task = session.dataTask(with: request) { data, response, error in
			guard let data = data, error == nil else
			{
				let extractedExpr: Response<ResponseDataType> = Response<ResponseDataType>(status: false, data: nil, error?.localizedDescription ?? "Unknown error occurred.")
				completion(extractedExpr)
				return
			}
			
			let json = String(data: data, encoding: .utf8)
			
			if let response = json?.decodeJson(Response<ResponseDataType>.self)
			{
				completion(response)
			}
			else if let json = json,
					let handler = self.fallbackErrorHandler,
					let errorResponse = json.decodeJson(Response<Int>.self)
			{
				handler(errorResponse)
			}
			else
			{
				completion(Response<ResponseDataType>(status: false, data: nil, "Server response could not be decoded from JSON format."))
			}
		}
		
		task.resume()
	}
	
	/**
		Gets the response asynchronously.
		- Parameter completion: The closure to call when the response is received.
	*/
	func getResponse() async throws -> Response<ResponseDataType>
	{
		if #available(iOS 15.0, *)
		{
			do
			{
				let (data, _) = try await session.data(for: request)
				let json = String(data: data, encoding: .utf8)
				return json?.decodeJson(Response<ResponseDataType>.self) ?? Response<ResponseDataType>(status: false, data: nil, "Response JSON could not be decoded.")
			}
			catch
			{
				return Response<ResponseDataType>(status: false, data: nil, error.localizedDescription)
			}
		}
		else
		{
			return Response<ResponseDataType>(status: false, data: nil, "Incompatible iOS platform.")
		}
	}
}
