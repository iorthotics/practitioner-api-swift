import Foundation

// Represents a Patient record.
public class Patient : Codable
{
	// Specifies the patient's GUID.
	public var guid: String? = nil
	
	// Specifies the patient's first name.
	public var firstName: String = ""
	
	// Specifies the patient's last name.
	public var lastName: String = ""
	
	// Specifies the patient's date of birth.
	public var dateOfBirth: String? = ""
	
	// Specifies the patient's height in cm.
	public var height: Int? = 0
	
	// Specifies the patient's weight in kg.
	public var weight: Int? = 0
	
	// Specifies the patient's gender ("Male" or "Female").
	public var gender: PatientGender = PatientGender.other
	
	// Specifies the creation date of the patient record.
	public var created: String?
	
	// Specifies the last update date of the patient record.
	public var updated: String?
	
	// Specifies the external ID string of the patient record.
	public var externalID: String?
	
	// Specifies whether the patient's name is a medical record number (last name).
	public var lastNameIsMedicalRecordNumber: Bool? = false
	
	// Gets the display name of the patient.
	public var displayName: String
	{
		get
		{
			if lastNameIsMedicalRecordNumber ?? false
			{
				return lastName
			}
			return "\(firstName) \(lastName)"
		}
	}
	
	// Gets the display name of the patient.
	public var displayNameAbbreviated: String
	{
		get
		{
			if lastNameIsMedicalRecordNumber ?? false
			{
				return lastName
			}
			return "\(firstName.trimmingCharacters(in: .whitespacesAndNewlines).prefix(1).uppercased()). \(lastName)"
		}
	}
	
	public enum CodingKeys: String, CodingKey
	{
		case guid
		case firstName = "first_name"
		case lastName = "last_name"
		case dateOfBirth = "birthdate"
		case height
		case weight
		case gender
		case created = "created_at"
		case updated = "updated_at"
		case externalID = "external_id"
		case lastNameIsMedicalRecordNumber = "is_mrn"
	}
	
	/**
		Initialises a new patient.
		- Parameter firstName: The patient's first name.
		- Parameter lastName: The patient's last name.
		- Parameter dateOfBirth: The patient's date of birth.
		- Parameter height: The patient's height in centimetres.
		- Parameter weight: The patient's weight in kilograms.
		- Parameter gender: The patient's gender.
	*/
	public init(firstName: String,
		 lastName: String,
		 dateOfBirth: Date,
		 height: Int = 0,
		 weight: Int = 0,
		 gender: PatientGender = PatientGender.other,
		 externalID: String? = nil)
	{
		self.firstName = firstName
		self.lastName = lastName
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd"
		self.dateOfBirth = formatter.string(from: dateOfBirth)
		self.height = height
		self.weight = weight
		self.gender = gender
		self.externalID = externalID
	}
	
	/**
		Initialises a new empty patient.
	*/
	public init()
	{
	}
	
	/**
		Gets the clinic's patient records based on optional filters.
		- Parameter api: The Practitioner API to use.
		- Parameter query: The patient query settings.
		- Parameter completion: The closure to call with the response.
	*/
	public static func get(api: PractitionerAPI,
					query: PatientsQuery,
					completion: @escaping (Response<RecordSet<Patient>>) -> ())
	{
		api.getPatients(query: query, completion: completion)
	}
	
	/**
		Creates the patient record.
		- Parameter api: The Practitioner API to use.
		- Parameter completion: The closure to call with the response.
	*/
	public func create(api: PractitionerAPI,
				completion: @escaping (Response<RecordSet<Patient>>) -> ())
	{
		api.createPatient(patient: self, completion: completion)
	}
	
	/**
		Updates the patient record.
		- Parameter api: The Practitioner API to use.
		- Parameter completion: The closure to call with the response.
	*/
	public func update(api: PractitionerAPI,
				completion: @escaping (Response<RecordSet<Patient>>) -> ())
	{
		api.updatePatient(patient: self, completion: completion)
	}
	
	/**
		Determines if the patient is a match with another.
		- Parameter patient: The patient to compare with.
	*/
	public func isMatchWith(_ patient: Patient) -> Bool
	{
		return guid == patient.guid &&
			firstName == patient.firstName &&
			lastName == patient.lastName &&
			dateOfBirth == patient.dateOfBirth &&
			gender == patient.gender &&
			externalID == patient.externalID
	}
}
