// Represents a list of orders created.
public class OrdersCreated : Codable
{
	// Specifies the order numbers that were created.
	public var orderNumbers: [String] = []
	
	public enum CodingKeys: String, CodingKey
	{
		case orderNumbers = "order_numbers"
	}
}
