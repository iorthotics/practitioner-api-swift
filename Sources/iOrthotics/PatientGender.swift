// Represents a patient's gender.
public enum PatientGender: String, Codable
{
	case notSpecified = ""
    case male = "Male"
    case female = "Female"
	case other = "Other"
	case nonBinary = "Non-Binary"
	case transgender = "Transgender"
	case neutral = "Neutral"
	case unspecified = "Unspecified"
}
