import Foundation

// Represents a Print Mill/Shell job.
public class PrintMillShellJob : Codable
{
	// Specifies the model file being manufactured.
	public var file: String = ""
	
	// Specifies the EVA density.
	public var evaDensity: EVADensity? = nil
	
	// Specifies the engraving test.
	public var engravingText: String? = nil
	
	// Specifies the reference number for the order.
	public var referenceNumber: String? = nil
	
	// Specifies the number of pairs being manufactured.
	public var pairs: Int = 1
	
	public enum CodingKeys: String, CodingKey
	{
		case file
		case evaDensity = "eva_density"
		case engravingText = "engraving_text"
		case referenceNumber = "reference_number"
		case pairs
	}
}
