public enum OrderStatus : String, Codable
{
	case orderReceived = "Order Received"
	case incompleteOrder = "Incomplete Order"
	case preDesign = "Pre-Design"
	case changesRequired = "Changes Required"
	case waitingPayment = "Waiting Payment"
	case designApproval = "Design Approval"
	case designed = "Designed"
	case nested = "Nested"
	case machining = "Machining"
	case printing = "Printing"
	case cooling = "Cooling"
	case finishing = "Finishing"
	case complete = "Complete"
	case readyToShip = "Ready To Ship"
	case shipped = "Shipped"
	case cancelled = "Cancelled"
	
	var string: String
	{
		get
		{
			return self.rawValue
		}
	}
}
