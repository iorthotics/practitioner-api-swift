import Foundation
#if !os(macOS)
import MobileCoreServices
#endif

// Represents a multipart/form-data content body.
class MultipartFormData
{
	let boundary = String(format: "%08X%08X", arc4random(), arc4random())
	private lazy var crlf = "\r\n".data(using: .utf8)!
	private lazy var formData = Data()
	private var fileIndex = 0
	
	// Gets the Content-Type header value for the request.
	var contentTypeHeaderValue: String
	{
		return "multipart/form-data; boundary=\(boundary)"
	}
	
	/**
		Adds data to the form data.
		- Parameter data: The data to append to the body.
		- Parameter withHttpHeaders: The HTTP headers to add for the data.
	*/
	func add(_ data: Data, withHttpHeaders: [String: String])
	{
        formData.append("--\(boundary)\r\n".data(using: .utf8)!)
		
		withHttpHeaders.forEach { (key: String, value: String) in
			self.formData.append("\(key): \(value)\r\n".data(using: .utf8)!)
		}
		
		self.formData.append(crlf)
		self.formData.append(data)
		self.formData.append(crlf)
	}
	
	/**
		Adds a named value to the form data.
		- Parameter value: The string value.
		- Parameter withName: The name of the value.
	*/
	func addValue(_ value: String, withName: String)
	{
		add(value.data(using: .utf8)!, withHttpHeaders: [
			"Content-Disposition": "form-data; name=\"\(withName)\""
		])
	}
	
	/**
		Adds a file to the form data.
		- Parameter url: The file URL.
		- Parameter name: Optional. The form-data name.
		- Parameter filename: The filename to assign to the form-data, otherwise the URL's basename.
		- Parameter contentType: The content mime-type, otherwise a pre-determined mime-type.
	*/
	func addFile(url: URL, name: String? = nil, filename: String? = nil, contentType: String? = nil) throws
	{
		let headers = [
			"Content-Disposition": "attachment; name=\"\(name ?? "file\(fileIndex)")\"; filename=\"\(filename ?? url.lastPathComponent)\"",
			"Content-Type": contentType ?? mimeType(for: url.pathExtension)
		]
		
		if (name == nil)
		{
			fileIndex += 1
		}
		
		add(try Data(contentsOf: url), withHttpHeaders: headers)
	}
    
    func finalise() -> Data
	{
        formData.append("--\(boundary)--\r\n".data(using: .utf8)!)
        return formData
    }
	
	/**
		Determines the content mime-type for a file extension.
		- Parameter pathExtension: The file's extension.
	*/
	private func mimeType(for pathExtension: String) -> String
	{
		guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as CFString, nil)?.takeRetainedValue() else
		{
			return "application/octet-stream"
		}
		
		let contentTypeCString = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue()
		return (contentTypeCString as String?) ?? "application/octet-stream"
	}
}
