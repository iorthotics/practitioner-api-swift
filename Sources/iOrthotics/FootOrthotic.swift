import Foundation

// Represents a Foot Orthotic order.
public class FootOrthotic : Codable
{
	// Specifies the type of foot orthotic being manufactured.
	public var type: String? = nil
	
	// Specifies whether the feet are symmetrical.
	public var symmetricalFeet: String? = "Yes"
	
	// Specifies the type of delivery.
	public var delivery: Delivery? = .standard
	
	// Specifies the type of shell.
	public var shellType: String? = nil
	
	// Specifies the name of the left scan file.
	public var scanFileLeft: String? = nil
	
	// Specifies the name of the right scan file.
	public var scanFileRight: String? = nil
	
	// Specifies the foot orthotic instructions.
	public var instructions: String? = nil
	
	// Specifies the design instructions.
	public var instructionsToDesigner: String? = nil
	
	public enum CodingKeys: String, CodingKey
	{
		case type
		case symmetricalFeet = "symmetrical_feet"
		case delivery
		case shellType = "shell_type"
		case scanFileLeft = "scan_left"
		case scanFileRight = "scan_right"
		case instructions
		case instructionsToDesigner = "instructions_to_designer"
	}
}
