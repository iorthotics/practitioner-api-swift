public enum EVADensity : String, Codable
{
	case Soft = "soft"
	case Medium = "medium"
	case Hard = "hard"
}
