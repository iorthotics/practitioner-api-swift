//
//  File.swift
//  
//
//  Created by Nick Bedford on 12/9/2023.
//

import Foundation

public class RefreshKey : Codable
{
	// Specifies the practitioner's new API key.
	public var apiKey: String
	
	// Optionally specifies the expiry timestamp of the API key.
	public var apiKeyExpires: Int
	
	public enum CodingKeys: String, CodingKey
	{
		case apiKey = "api_key"
		case apiKeyExpires = "api_key_expires"
	}
}
