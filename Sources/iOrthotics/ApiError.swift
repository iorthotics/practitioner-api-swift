public enum ApiError : Error
{
	case dataError
	case missingArgument(named: String)
	case incompatiblePlatform
	case invalidArgument(named: String)
}
