import Foundation

// Represents a query for patients.
public class PatientScansQuery
{
	// Specifies the patient GUID to retrieve.
	public var patientGuid: String
	
	// Optional. The patient scan's GUID to filter to.
	public var patientScanGuid: String? = nil
	
	// Optional. The patient scan group's GUID to filter to.
	public var scanGroupGuid: String? = nil
	
	/**
		Initialises a patient scans query.
		- Parameter patientGuid: The patient's GUID.
		- Parameter patientScanGuid: Optional. The patient scan's GUID to filter to.
		- Parameter scanGroupGuid: Optional. The patient scan group's GUID to filter to.
	*/
	public init(patientGuid: String, _ patientScanGuid: String? = nil, _ scanGroupGuid: String? = nil)
	{
		self.patientGuid = patientGuid
		self.patientScanGuid = patientScanGuid
		self.scanGroupGuid = scanGroupGuid
	}
}
