// Represents a delivery type.
public enum Delivery: String, Codable
{
	case notSpecified = ""
    case standard = "Standard"
    case express = "Express"
}
