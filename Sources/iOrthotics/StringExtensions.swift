import Foundation

extension String
{
	var somethingOrNil: String?
	{
		get
		{
			self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ? nil : self
		}
	}
	
	/**
		Decodes the string from a base-64 representation to UTF-8.
	*/
	var base64EncodedStringToUTF: String?
	{
		guard let data = Data(base64Encoded: self) else
		{
			return nil
		}
		return String(data: data, encoding: .utf8)
	}
	
	/**
		Encodes the string into a base-64 representation.
	*/
	var utf8ToBase64EncodedString: String
	{
		return Data(self.utf8).base64EncodedString()
	}
	
	/**
		Decodes the string as JSON to the specified type.
	*/
	func decodeJson<T>(_ type: T.Type) -> T? where T : Decodable
	{
		do
		{
			guard let data = self.data(using: String.Encoding.utf8) else
			{
				return nil
			}
			return try JSONDecoder().decode(type, from: data)
		}
		catch let DecodingError.dataCorrupted(context)
		{
			print(context)
		}
		catch let DecodingError.keyNotFound(key, context)
		{
			print("Key '\(key)' not found:", context.debugDescription)
			print("codingPath:", context.codingPath)
		}
		catch let DecodingError.valueNotFound(value, context)
		{
			print("Value '\(value)' not found:", context.debugDescription)
			print("codingPath:", context.codingPath)
		}
		catch let DecodingError.typeMismatch(type, context)
		{
			print("Type '\(type)' mismatch:", context.debugDescription)
			print("codingPath:", context.codingPath)
		}
		catch
		{
			print(error)
		}
		return nil
	}
	
	/**
		Converts the string to a date based on a format.
		- Parameter format: The format string to describe the string's date components.
	*/
	func toDate(_ format: String = "yyyy-MM-dd") -> Date?
	{
		let formatter = DateFormatter()
		formatter.dateFormat = format
		return formatter.date(from: self)
	}
}
