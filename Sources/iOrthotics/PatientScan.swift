import Foundation

// Represents a Patient Scan record.
public class PatientScan : Codable
{
	// Specifies the patient scan's GUID.
	public var guid: String? = nil
	
	// Specifies the patient scan's group GUID.
	public var groupGuid: String? = nil
	
	// Specifies the creation date of the patient scan record.
	public var created: String?
	
	// Specifies the name of the file.
	public var filename: String? = nil
	
	// Specifies the URL to download the file from.
	public var url: String? = nil
	
	// Specifies the timestamp when the download URL expires.
	public var urlExpires: Int? = 0
	
	// Gets the URL expiry date.
	public var urlExpiryDate: Date?
	{
		return urlExpires != nil ? nil :
			Date(timeIntervalSince1970: TimeInterval(Double(urlExpires!)))
	}
	
	public enum CodingKeys: String, CodingKey
	{
		case guid
		case groupGuid = "group_guid"
		case created = "created_at"
		case filename
		case url
		case urlExpires = "url_expires"
	}
}
