import Foundation
import UIKit

// Represents the interface to the Practitioner API.
public class PractitionerAPI
{
	// Specifies the server to communicate with.
	public let server: Server
	
	private var _apiKey: String
	
	// Specifies the API key used to authenticate each request.
	public var apiKey: String
	{
		get
		{
			_apiKey
		}
		set(value)
		{
			_apiKey = value
			server.defaultHeaders["Authorization"] = "Basic \("api:\(_apiKey)".utf8ToBase64EncodedString)"
		}
	}
	
	public var delegate: RequestDelegate? = nil
	public var clientApplication: ClientApplication? = nil
	
	// Specifies the optional API key expiry timestamp.
	public var apiKeyExpires: Int? = nil
	
	public static var developmentServer = Server(name: "iOrthotics Development", serverId: "local", baseUri: "https://dev.iorthotics.com.au/practitioner/api")
	public static var stagingServer = Server(name: "iOrthotics Staging", serverId: "staging", baseUri: "https://staging.iorthotics.com.au/practitioner/api")
	public static var australiaServer = Server(name: "iOrthotics Australia", serverId: "aus", baseUri: "https://accounts.iorthotics.com.au/practitioner/api")
	public static var usaServer = Server(name: "iOrthotics USA", serverId: "usa", baseUri: "https://usa.iorthotics.com/practitioner/api", USDateFormat: true)
	public static var herscoStagingServer = Server(name: "Hersco Ortho Labs [Staging]", serverId: "herscoStaging", baseUri: "https://hersco.iorthotics.com/practitioner/api", USDateFormat: true)
	public static var herscoProductionServer = Server(name: "Hersco Ortho Labs", serverId: "herscoProduction", baseUri: "https://portal.hersco.com/practitioner/api", USDateFormat: true)
	
	// Specifies the iOrthotics production servers.
	public static var productionServers: [Server] =
	[
		australiaServer,
		usaServer,
		herscoProductionServer
	]
	
	// Specifies the iOrthotics testing servers.
	public static var testServers: [Server] =
	[
		stagingServer,
		developmentServer,
		herscoStagingServer
	]
	
	public static var defaultDeviceDescription: String
	{
		get
		{
			"\(UIDevice.current.model) (\(UIDevice.current.systemName) \(UIDevice.current.systemVersion))"
		}
	}
	
	// Specifies all of the iOrthotics servers including the test servers.
	public static var allServers: [Server] = PractitionerAPI.productionServers + PractitionerAPI.testServers
	
	public var fallbackErrorHandler: ((Response<Int>) -> Void)? = nil
	
	public var secondsUntilApiKeyExpires: TimeInterval?
	{
		get
		{
			return apiKeyExpires != nil ?
				(Date(timeIntervalSince1970: TimeInterval(apiKeyExpires!)).timeIntervalSinceNow) :
				nil
		}
	}
	
	/**
		Initialises a new instance of the API.
	
		- Parameter server: The API server to communicate with.
		- Parameter apiKey: The practitioner's API key to authenticate each request with.
	**/
	public init(server: Server,
				apiKey: String,
				apiKeyExpires: Int? = nil)
	{
		self.server = Server(server: server)
		self._apiKey = apiKey
		self.server.defaultHeaders["Authorization"] = "Basic \("api:\(apiKey)".utf8ToBase64EncodedString)"
		self.server.defaultHeaders["Accept"] = "application/json"
		self.server.defaultHeaders["Cache-Control"] = "no-store"
		self.apiKeyExpires = apiKeyExpires
	}
	
	public func updateAuthentication(for request: RequestBase) -> Void
	{
		request.request.setValue("Basic \("api:\(apiKey)".utf8ToBase64EncodedString)", forHTTPHeaderField: "Authorization")
	}
	
	public func checkOkay(completion: @escaping (Response<[String:String]>) -> ())
	{
		let request = server.createRequest(
			responseType: [String:String].self,
			path: "/",
			fallbackErrorHandler: fallbackErrorHandler)
		
		request.executeTask(completion: completion)
	}
	
	/**
		Authenticates a practitioner, returning their details and API key if authenticated.
		- Parameter username: The practitioner's username.
		- Parameter password: The practitioner's password.
		- Parameter applicationKey: The application key.
	*/
	public func authenticatePractitioner(username: String,
										 password: String,
										 applicationKey: String,
										 deviceGuid: String? = nil,
										 deviceDescription: String? = nil,
										 totpCode: String? = nil,
										 completion: @escaping (Response<AuthenticationInfo>) -> ())
	{
		let content = MultipartFormData()
		content.addValue(username, withName: "username")
		content.addValue(password, withName: "password")
		content.addValue(applicationKey, withName: "key")
			
		if let device = deviceGuid
		{
			content.addValue(device, withName: "device_guid")
			
			let description = deviceDescription ?? PractitionerAPI.defaultDeviceDescription
			content.addValue(description, withName: "device_description")
			
			if let totp = totpCode?.somethingOrNil
			{
				content.addValue(totp, withName: "totp")
			}
		}
		
		let request = server.createRequest(
			responseType: AuthenticationInfo.self,
			path: "/key",
			method: "POST",
			query: nil,
			content: content.finalise(),
			fallbackErrorHandler: fallbackErrorHandler)
		let _ = request.addValue(content.contentTypeHeaderValue, forHTTPHeaderField: "Content-Type")
		
		approve(request, isAuthenticationRequest: true) {
			request.executeTask(completion: completion)
		}
	}
	
	private func approve<ResponseType>(_ request: Request<ResponseType>, isAuthenticationRequest auth: Bool = false, _ completion: @escaping () -> Void)
	{
		guard let delegate = delegate else
		{
			completion()
			return
		}
		
		if delegate.requestShouldContinue(to: request, method: request.method, via: self, isAuthenticationRequest: auth, async: completion)
		{
			completion()
			return
		}
	}
	
	public func refreshTemporaryApiKey(deviceGuid: String, completion: @escaping (Response<RefreshKey>) -> Void)
	{
		let params: [String:String?] = [ "device_guid": deviceGuid ]
		
		let request = server.createRequest(
			 responseType: RefreshKey.self,
			 path: "/key/refresh",
			 method: "PATCH",
			 content: params.formUrlEncodedContent,
			 fallbackErrorHandler: fallbackErrorHandler)
		let _ = request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
		
		let complete: (Response<RefreshKey>) -> Void = { response in
			if response.status, let data = response.data
			{
				self.apiKey = data.apiKey
				self.apiKeyExpires = data.apiKeyExpires
				if let app = self.clientApplication {
					ClientApplication.delegate?.clientApplication(apiChangedFor: app)
				}
			}
			completion(response)
		}
		
		approve(request, isAuthenticationRequest: true) {
			request.executeTask(completion: complete)
		}
	}
	
	public func revokeTemporaryApiKey(completion: @escaping (Response<Int>) -> Void)
	{
		
		let request = server.createRequest(
			 responseType: Int.self,
			 path: "/key",
			 method: "DELETE",
			 fallbackErrorHandler: fallbackErrorHandler)
			
		approve(request, isAuthenticationRequest: true) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Gets the practitioner's linked accounts for user switching.
		- Parameter completion: The closure to call with the response.
	*/
	public func getLinkedUsers(completion: @escaping (Response<[LinkedUser]>) -> ())
	{
		let request = server.createRequest(
				responseType: [LinkedUser].self,
				path: "/linked-users")
		
		approve(request, isAuthenticationRequest: true) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Switches the user to a different linked user account. If successful, this revokes the current API
	    key being used to authenticate this request and issues a new one for the linked user.
		- Parameter linkedUserGuid: The GUID identifier of the linked user account.
		- Parameter completion: The closure to call with the response.
	*/
	public func switchUser(deviceGuid: String,
						   deviceDescription: String?,
						   linkedUserGuid: String,
						   totp: String?,
						   completion: @escaping (Response<AuthenticationInfo>) -> ())
	{
		var content: [String:String?] = [:]
		content["device_guid"] = deviceGuid
		content["device_description"] = deviceDescription ?? PractitionerAPI.defaultDeviceDescription
		
		if let totp = totp?.somethingOrNil
		{
			content["totp"] = totp
		}
		
		let request = server.createRequest(
				responseType: AuthenticationInfo.self,
				path: "/switch-user/\(linkedUserGuid)",
				method: "POST",
				content: content.formUrlEncodedContent)
		
		approve(request, isAuthenticationRequest: true) {
			request.executeTask(completion: completion)
		}
	}
	
	public func createSessionUrl(url: String,
								 completion: @escaping (Response<SessionUrl>) -> ())
	{
		let content = MultipartFormData()
		content.addValue(url, withName: "url")
		
		let request = server.createRequest(
			responseType: SessionUrl.self,
			path: "/session-url",
			method: "POST",
			query: nil,
			content: content.finalise(),
			headers: [
				"Content-Type": content.contentTypeHeaderValue
			])
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Gets the clinic's orders based on optional filters.
		- Parameter query: The order query settings.
		- Parameter completion: The closure to call with the response.
	*/
	public func getGenders(completion: @escaping (Response<[String]>) -> ())
	{
		let request = server.createRequest(
				responseType: [String].self,
				path: "/genders")
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Gets the clinic's orders based on optional filters.
		- Parameter query: The order query settings.
		- Parameter completion: The closure to call with the response.
	*/
	public func getOrders(query: OrdersQuery, completion: @escaping (Response<RecordSet<Order>>) -> ())
	{
		let parameters = [
			"page": String(query.page),
			"length": String(query.length),
			"scope": query.allClinicOrders ? "clinic" : "user"
		] as [String: String?]
		
		let request = server.createRequest(
			responseType: RecordSet<Order>.self,
			path: "/orders\(query.orderNumber != nil ? "/\(query.orderNumber!)" : "")",
			method: "GET",
			query: parameters,
			content: nil)
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Gets the clinic's patient records based on optional filters.
		- Parameter query: The patient query settings.
		- Parameter completion: The closure to call with the response.
	*/
	public func getPatients(query: PatientsQuery, completion: @escaping (Response<RecordSet<Patient>>) -> ())
	{
		var parameters = [
			"page": String(query.page),
			"length": String(query.length),
			"search": query.search
		] as [String: String?]
		
		if let sortBy = query.sortBy {
			parameters["sort_by"] = sortBy
			parameters["sort_dir"] = query.sortByDesc ? "desc" : "asc"
		}
		
		let request = server.createRequest(
			responseType: RecordSet<Patient>.self,
			path: "/patients\(query.patientGuid != nil ? "/\(query.patientGuid!)" : "")",
			method: "GET",
			query: parameters,
			content: nil)
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Creates a new patient record.
		- Parameter patient: The patient record to create.
		- Parameter completion: The closure to call with the response.
	*/
	public func createPatient(patient: Patient, completion: @escaping (Response<RecordSet<Patient>>) -> ())
	{
		let request = server.createRequest(
			responseType: RecordSet<Patient>.self,
			path: "/patients",
			method: "POST",
			query: nil,
			content: patient.jsonData)
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Updates an existing patient record.
		- Parameter patient: The patient record to update.
		- Parameter completion: The closure to call with the response.
	*/
	public func updatePatient(patient: Patient, completion: @escaping (Response<RecordSet<Patient>>) -> ())
	{
		let request = server.createRequest(
			responseType: RecordSet<Patient>.self,
			path: "/patients/\(patient.guid!)",
			method: "PATCH",
			query: nil,
			content: patient.jsonData)
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Gets a patient's scans optionally filtered to a specific scan.
		- Parameter query: The patient scan query settings.
		- Parameter completion: The closure to call with the response.
	*/
	public func getPatientScans(query: PatientScansQuery, completion: @escaping (Response<RecordSet<Patient>>) -> ())
	{
		let request = server.createRequest(
			responseType: RecordSet<Patient>.self,
			path: "/patients/\(query.patientGuid)/scans\(query.patientScanGuid != nil ? "/\(query.patientScanGuid!)" : "")",
			method: "GET",
			query: [ "group": query.scanGroupGuid ])
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Uploads one or more scan files to a patient, optionally grouped by a GUID.
		- Parameter patientGuid: The patient's GUID.
		- Parameter scanGroupGuid: Optional. The GUID of the files to be grouped together with. This can be an existing group GUID or a new one.
		- Parameter files: The files to upload to the patient's scans.
		- Parameter completion: The closure to call with the response.
	*/
	public func upload(scans: [PatientScanUpload],
					   patientGuid: String,
					   scanGroupGuid: UUID?,
					   completion: @escaping (Response<RecordSet<PatientScan>>) -> ())
	{
		let data = MultipartFormData()
		if (scanGroupGuid != nil)
		{
			data.addValue(scanGroupGuid!.uuidString.lowercased(), withName: "group_guid")
		}
		
		do
		{
			try scans.forEach { (file: PatientScanUpload) in
				do
				{
					try data.addFile(url: file.dataUrl)
				}
				catch
				{
					throw error
				}
			}
		}
		catch ApiError.dataError
		{
			completion(Response<RecordSet<PatientScan>>(status: false, data: nil, "An error occurred when generating the request."))
			return
		}
		catch
		{
			completion(Response<RecordSet<PatientScan>>(status: false, data: nil, "An error occurred when reading a file to be uploaded."))
			return
		}
		
		let request = server.createRequest(responseType: RecordSet<PatientScan>.self,
										   path: "/patients/\(patientGuid)/scans",
										   method: "POST",
										   query: nil,
										   content: data.finalise())
		let _ = request.addValue(data.contentTypeHeaderValue, forHTTPHeaderField: "Content-Type")
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Uploads one or more scan files to a patient, optionally grouped by a GUID, sending them one request at a time.
	 	- Parameter scansSeparately: The files to upload to the patient's scans.
		- Parameter patientGuid: The patient's GUID.
		- Parameter scanGroupGuid: Optional. The GUID of the files to be grouped together with. This can be an existing group GUID or a new one.
		- Parameter completionPerUpload: The closure to call with the response. If the upload failed, the return value should indicate whether to
	 									 attempt the upload again.
	*/
	public func upload(scansSeparately scans: [PatientScanUpload],
					   patientGuid: String,
					   scanGroupGuid: UUID?,
					   completionPerUpload: @escaping (ProgressCounter, PatientScanUpload, Response<RecordSet<PatientScan>>) -> Bool)
	{
		upload(scansSeparately: scans,
			   patientGuid: patientGuid,
			   scanGroupGuid: scanGroupGuid,
			   withProgress: ProgressCounter(complete: 0, total: scans.count),
			   completionPerUpload: completionPerUpload)
	}
	
	private func upload(scansSeparately scans: [PatientScanUpload],
						patientGuid: String,
						scanGroupGuid: UUID?,
						withProgress progress: ProgressCounter,
						completionPerUpload: @escaping (ProgressCounter, PatientScanUpload, Response<RecordSet<PatientScan>>) -> Bool)
	{
		guard scans.count > 0,
			  let scanToUpload = scans.first else
		{
			return
		}
		
		let remaining = scans.suffix(from: 1)
		
		upload(scans: [scanToUpload],
			   patientGuid: patientGuid,
			   scanGroupGuid: scanGroupGuid) { response in
			
			if !response.status
			{
				if completionPerUpload(progress, scanToUpload, response)
				{
					self.upload(scansSeparately: scans,
								patientGuid: patientGuid,
								scanGroupGuid: scanGroupGuid,
								withProgress: progress,
								completionPerUpload: completionPerUpload)
				}
				return
			}
			
			let progress = progress.advanced
			
			guard completionPerUpload(progress, scanToUpload, response),
				  !remaining.isEmpty else
			{
				return
			}
			
			self.upload(scansSeparately: Array(remaining),
						patientGuid: patientGuid,
						scanGroupGuid: scanGroupGuid,
						withProgress: progress,
						completionPerUpload: completionPerUpload)
		}
	}
	
	/**
		Deletes a patient scan file.
		- Parameter patientGuid: The patient's GUID.
		- Parameter scanGuid: The patient scan GUID.
		- Parameter completion: The closure to call with the response.
	*/
	public func deletePatientScan(patientGuid: String, scanGuid: String, completion: @escaping (Response<Int>) -> ())
	{
		let request = server.createRequest(
			responseType: Int.self,
			path: "/patients/\(patientGuid)/scans/\(scanGuid)",
			method: "DELETE")
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Generates the multipart/form-data content for a shell upload.
	*/
	private func generateShellOrderContent(shells: [ShellUpload]) throws -> MultipartFormData
	{
		let data = MultipartFormData()
		var i = 0
		
		try shells.forEach { (shell: ShellUpload) in
			shell.fileKey = "file\(i)"
			i += 1
			
			if (shell.fileUrl == nil)
			{
				throw ApiError.missingArgument(named: "shell.fileUrl")
			}
			
			try data.addFile(url: shell.fileUrl!, name: shell.fileKey)
		}
		
		let list = ShellUploadList(shells: shells)
		
		data.add(list.jsonData!, withHttpHeaders: [
			"Content-Disposition": "form-data; name=\"data\"",
			"Content-Type": "application/json"
		])
		
		return data
	}
	
	/**
		Creates a series of 3D or EVA Shell orders based on a list of uploads and pair counts.
		- Parameter shells: The 3D shell uploads to order.
		- Parameter completion: The closure to call with the response.
	*/
	private func createShellOrder(shells: [ShellUpload], type: String, completion: @escaping (Response<OrdersCreated>) -> ())
	{
		do
		{
			let data = try generateShellOrderContent(shells: shells)
			let request = server.createRequest(responseType: OrdersCreated.self,
											   path: "/order/\(type)",
											   method: "POST",
											   query: nil,
											   content: data.finalise())
			let _ = request.addValue(data.contentTypeHeaderValue, forHTTPHeaderField: "Content-Type")
			
			approve(request) {
				request.executeTask(completion: completion)
			}
		}
		catch ApiError.missingArgument
		{
			completion(Response<OrdersCreated>(status: false, data: nil, "An error occurred when generating the request."))
			return
		}
		catch ApiError.dataError
		{
			completion(Response<OrdersCreated>(status: false, data: nil, "An error occurred when generating the request."))
			return
		}
		catch
		{
			completion(Response<OrdersCreated>(status: false, data: nil, "An error occurred when reading a file to be uploaded."))
			return
		}
	}
	
	/**
		Creates a series of 3D Shell orders based on a list of uploads and pair counts.
		- Parameter shells: The 3D shell uploads to order.
		- Parameter completion: The closure to call with the response.
	*/
	public func create3DShellOrder(shells: [ShellUpload], completion: @escaping (Response<OrdersCreated>) -> ())
	{
		createShellOrder(shells: shells, type: "3d-shell", completion: completion)
	}
	
	/**
		Creates a series of EVA Shell orders based on a list of uploads and pair counts.
		- Parameter shells: The EVA shell uploads to order.
		- Parameter completion: The closure to call with the response.
	*/
	public func createEVAShellOrder(shells: [ShellUpload], completion: @escaping (Response<OrdersCreated>) -> ())
	{
		createShellOrder(shells: shells, type: "eva-shell", completion: completion)
	}
	
	/**
		Records a TM3DSCAN or TM3DSCANAFO pending clinic line item.
	*/
	public func recordTechMed3DScan(patientGuid: String, quantity: Int = 1, completion: @escaping (Response<Int>) -> (), type: TechMed3DScanType = .footPlantar)
	{
		let request = server.createRequest(
			responseType: Int.self,
			path: "/record-tm3d-scan/\(patientGuid)/\(quantity)",
			method: "POST",
			query: [ "type": type.rawValue ])
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Creates a custom event tied to the current practitioner for the most appropriate receiver for the event type.
	    - Parameter type: The type of event to create.
	 	- Parameter description: The description of the event.
	 	- Parameter data: Any additional data required to store.
	 */
	public func createCustomEvent<TData : Encodable>(type: CustomEventType, description: String, data: TData? = nil, completion: @escaping (Response<Int>) -> ())
	{
		let formData = MultipartFormData()
		formData.addValue(description, withName: "description")
		
		var jsonData = ""
		
		if let data = data,
		   let encoded = try? JSONEncoder().encode(data),
		   let json = String(data: encoded, encoding: .utf8)
		{
			jsonData = json
		}
		
		formData.addValue(jsonData, withName: "data")
		let content = formData.finalise()
		
		let request = server.createRequest(
			responseType: Int.self,
			path: "/custom-event/\(type.rawValue)",
			method: "POST",
			content: content,
			fallbackErrorHandler: fallbackErrorHandler,
			headers: [
				"Content-Type": formData.contentTypeHeaderValue
			])
	   
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
	
	/**
		Uploads one of more log files to the server.
		- Parameter logFiles: The list of log file URLs.
		- Parameter completion: The closure to call with the response.
	*/
	public func upload(logFiles: [URL],
					     completion: @escaping (Response<Bool>) -> ())
	{
		let data = MultipartFormData()
		
		do
		{
			try logFiles.filter({ $0.pathExtension == "log" }).forEach { file in
				do
				{
					try data.addFile(url: file)
				}
				catch
				{
					throw error
				}
			}
		}
		catch ApiError.dataError
		{
			completion(Response<Bool>(status: false,
									  data: nil,
									  "An error occurred when generating the request."))
			return
		}
		catch
		{
			completion(Response<Bool>(status: false,
														data: nil,
														"An error occurred when reading a file to be uploaded."))
			return
		}
		
		let request = server.createRequest(responseType: Bool.self,
										   path: "/logs",
										   method: "POST",
										   query: nil,
										   content: data.finalise())
		
		let _ = request.addValue(data.contentTypeHeaderValue, forHTTPHeaderField: "Content-Type")
		
		approve(request) {
			request.executeTask(completion: completion)
		}
	}
}
