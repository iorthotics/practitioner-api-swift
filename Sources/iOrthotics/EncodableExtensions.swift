import Foundation

extension Encodable
{
	// Gets the encodable object as JSON data.
	var jsonData: Data?
	{
		do
		{
			let encoder = JSONEncoder()
			return try encoder.encode(self)
		}
		catch
		{
			return nil
		}
	}
}
