import Foundation

// Represents a client application instance.
public class ClientApplication
{
	// Gets the application key used for authentication.
	public let applicationKey: String
	
	// Gets the server the application will communicate with.
	public let server: Server
	
	// Gets the authentication response.
	public private(set) var authenticationResponse: Response<AuthenticationInfo>? = nil
	
	// Gets the authenticated user information, if successful.
	public private(set) var user: AuthenticationInfo? = nil
	
	// Gets the practitioner API instance.
	public private(set) var api: PractitionerAPI? = nil
	{
		didSet
		{
			api?.clientApplication = self
			ClientApplication.delegate?.clientApplication(apiChangedFor: self)
		}
	}
	
	// Gets or sets the main client application instance.
	public static var main: ClientApplication? = nil
	{
		didSet
		{
			delegate?.mainClientApplication(wasReplacedWith: main)
		}
	}
	
	// Gets whether the authentication succeeded.
	public var authenticationSucceeded: Bool
	{
		get
		{
			return (authenticationResponse?.status ?? false) &&
				api != nil && user != nil
		}
	}
	
	public static var delegate: ClientApplicationDelegate? = nil
	
	/**
		Initialises a new client application.
		- Parameter server: The server the application will communicate with.
		- Parameter applicationKey: The appication key to use in authenticating
		the practitioner.
	*/
	public init(server: Server, applicationKey: String)
	{
		self.applicationKey = applicationKey
		self.server = server
	}
	
	/**
		Logs the user out.
	*/
	public func logout()
	{
		user = nil
		api = nil
		authenticationResponse = nil
	}
	
	/**
		Authenticates a practitioner.
		- Parameter username: The practitioner's username.
		- Parameter password: The practitioner's password.
		- Parameter completion: The closure to call when the authentication completes.
	*/
	public func authenticate(username: String,
							 password: String,
							 deviceGuid: String? = nil,
							 deviceDescription: String? = nil,
							 totpCode: String? = nil,
							 completion: @escaping (ClientApplication) -> ())
	{
		logout()
		
		let api = PractitionerAPI(server: server, apiKey: "")
		api.authenticatePractitioner(username: username,
									 password: password,
									 applicationKey: applicationKey,
									 deviceGuid: deviceGuid,
									 deviceDescription: deviceDescription,
									 totpCode: totpCode) { (response: Response<AuthenticationInfo>) in
			self.authenticationResponse = response
			
			if (self.authenticationResponse?.status ?? false)
			{
				self.user = response.data
				self.api = PractitionerAPI(server: self.server, apiKey: response.data?.apiKey ?? "", apiKeyExpires: response.data?.apiKeyExpires)
			}
			
			completion(self)
		}
	}
	
	/**
		Authenticates a practitioner.
		- Parameter username: The practitioner's username.
		- Parameter password: The practitioner's password.
		- Parameter completion: The closure to call when the authentication completes.
	*/
	public func switchUser(linkedUserGuid: String,
						   deviceGuid: String,
						   deviceDescription: String? = nil,
						   totpCode: String? = nil,
						   completion: @escaping (ClientApplication, Response<AuthenticationInfo>) -> ())
	{
		api?.switchUser(deviceGuid: deviceGuid,
						deviceDescription: deviceDescription,
						linkedUserGuid: linkedUserGuid,
						totp: totpCode, completion: { response in
						
			guard response.status else
			{
				completion(self, response)
				return
			}
			
			guard response.data?.username?.somethingOrNil != nil else
			{
				completion(self, Response<AuthenticationInfo>(status: false, data: nil, "Invalid response from server (missing username)."))
				return
			}
			
			self.user = response.data
			self.api = PractitionerAPI(server: self.server,
									   apiKey: response.data?.apiKey ?? "",
									   apiKeyExpires: response.data?.apiKeyExpires)
			
			completion(self, response)
			
		})
	}
	
	/**
		Sets the user to an offline copy.
		- Parameter user: The authentication information from a previous session.
	*/
	public func offline(user: AuthenticationInfo)
	{
		self.user = user
		api = nil
		authenticationResponse = Response(data: user)
	}
	
	/**
		Sets the user to an offline copy.
		- Parameter user: The authentication information from a previous session.
		- Parameter apiKey: The practitioner's API key to authenticate requests.
	*/
	public func online(user: AuthenticationInfo, apiKey: String, apiKeyExpires: Int? = nil)
	{
		self.user = user
		api = PractitionerAPI(server: server, apiKey: apiKey, apiKeyExpires: apiKeyExpires)
		api?.clientApplication = self
		authenticationResponse = Response(data: user)
	}
}
