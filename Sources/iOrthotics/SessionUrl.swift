import Foundation

// Represents a session url API response.
public class SessionUrl : Decodable
{
	// Specifies the entries for this page of the record set.
	public var url: String = ""
	
	// Specifies the page information for the record set.
	public var expires: Int = 0
	
	public init()
	{
	}
}
