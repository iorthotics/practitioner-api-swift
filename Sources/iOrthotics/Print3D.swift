import Foundation

// Represents a Print 3D order (O&P).
public class Print3D : Codable
{
	// Specifies the clinic name the order is destined for.
	public var clinicName: String? = nil
	
	// Specifies the practitioner who referred the patient.
	public var referringPractitioner: String? = nil
	
	// Specifies the devices being manufactured.
	public var devices: [Print3DJob]? = []
	
	public enum CodingKeys: String, CodingKey
	{
		case clinicName = "clinic_name"
		case referringPractitioner = "referring_practitioner"
		case devices
	}
}
