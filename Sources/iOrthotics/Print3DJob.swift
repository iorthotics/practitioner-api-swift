import Foundation

// Represents a Print 3D job.
public class Print3DJob : Codable
{
	// Specifies the file name to be printed.
	public var file: String = ""
	
	// Specifies the side of the foot.
	public var side: String? = nil
	
	// Specifies the number of pairs to print.
	public var pairs: Int = 1
	
	// Specifies the type of device.
	public var type: String? = nil
	
	// Specifies the notes about the devices.
	public var notes: String? = nil
}
