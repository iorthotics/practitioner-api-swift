// Represents a query for orders.
public class OrdersQuery
{
	// Specifies the zero-based page index.
	public var page: Int = 0
	
	// Specifies the length of each page.
	public var length: Int = 100
	
	// Optional. Specifies the order number to retrieve.
	public var orderNumber: String? = nil
	
	// Optional. Specifies whether to only retrieve the podiatrit's orders instead the entire clinic.
	public var allClinicOrders = true
	
	/**
		Initialises a new patients query.
		- Parameter page: The zero-based page index.
		- Parameter length: The length of each page.
		- Parameter orderNumber: Optional. The order number to retrieve.
	*/
	public init(page: Int = 0, orderNumber: String? = nil, allClinicOrders: Bool = true, length: Int = 100)
	{
		self.page = page
		self.length = length
		self.orderNumber = orderNumber
		self.allClinicOrders = allClinicOrders
	}
}
