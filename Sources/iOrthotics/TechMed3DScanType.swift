//
//  File.swift
//  
//
//  Created by Nick Bedford on 24/5/2023.
//

import Foundation

public enum TechMed3DScanType : String
{
	case footPlantar = "FootPlantar"
	case afo = "AFO"
}
