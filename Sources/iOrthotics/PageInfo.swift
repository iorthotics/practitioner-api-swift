import Foundation

// Specifies the page information for a record set.
public struct PageInfo : Decodable
{
	// Specifies the current page index.
	public var page: Int = 0
	
	// Specifies the previous page index if available.
	public var previous: Int? = nil
	
	// Specifies the next page index if available.
	public var next: Int? = nil
	
	// Specifies the length of each page returned.
	public var length: Int = 0
	
	// Specifies the total number of records in the entire record set.
	public var total: Int = 0
}
