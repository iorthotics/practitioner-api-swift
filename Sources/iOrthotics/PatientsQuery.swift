// Represents a query for patients.
public class PatientsQuery
{
	// Specifies the zero-based page index.
	public var page: Int = 0
	
	// Specifies the length of each page.
	public var length: Int = 100
	
	// Optional. Specifies the patient GUID to retrieve.
	public var patientGuid: String? = nil
	
	// Optional. Specifies the patient search query.
	public var search: String? = nil
	
	// Optional. Specifies the sort by field.
	public var sortBy: String? = nil
	
	// Optional. Specifies the direction of the sort by field as descending.
	public var sortByDesc: Bool = false
	
	/**
		Initialises a new patients query.
		- Parameter page: The zero-based page index.
		- Parameter length: The length of each page.
		- Parameter patientGuid: Optional. The patient GUID to retrieve.
		- Parameter search: Optional. The patient search query.
	*/
	public init(page: Int = 0, patientGuid: String? = nil, search: String? = nil, length: Int = 100)
	{
		self.page = page
		self.length = length
		self.patientGuid = patientGuid
		self.search = search
	}
}
