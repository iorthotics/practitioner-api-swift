//
//  File.swift
//  
//
//  Created by Nick Bedford on 12/9/2023.
//

import Foundation

public class RequestBase
{
	public var request: URLRequest
	public var session: URLSession
	public let fallbackErrorHandler: ((Response<Int>) -> Void)? = nil
	public var originalPath: String? = nil
   
	public var url: URL?
	{
		get
		{
			return request.url
		}
	}
   
	public var method: String
	{
		get
		{
			return request.httpMethod ?? "GET"
		}
	}
	
	/**
		Initialises a new request.
		- Parameter request: The URL request to execute.
	*/
	public init(request: URLRequest,
		 session: URLSession? = nil,
		 fallbackErrorHandler: ((Response<Int>) -> Void)? = nil,
		 originalPath: String? = nil)
	{
		self.request = request
		self.session = session ?? Server.defaultUrlSession
		self.originalPath = originalPath
	}
}
