import Foundation

// Represents a standard JSON response from the API.
public class Response<DataType: Decodable> : Decodable
{
	// Specifies whether the request succeeded or not.
	public var status: Bool = true
	
	// Specifies the reason for an error if the request failed.
	public var reason: String? = nil
	
	// Specifies the data in the response if any was provided.
	public var data: DataType? = nil
	
	// Specifies any errors in the request.
	public var errors: [String: String?]? = nil
	
	/**
		Initialises a new response.
		- Parameter status: Whether the request succeeded.
		- Parameter data: Optional. The data in the response.
		- Parameter reason: Optional. The reason for an error if the request failed.
	*/
	public init(status: Bool, data: DataType? = nil, _ reason: String? = nil, errors: [String: String]? = nil)
	{
		self.status = status
		self.data = data
		self.reason = reason
		self.errors = errors
	}
	
	/**
		Initialises a new successful response with no data.
	*/
	public convenience init()
	{
		self.init(status: true)
	}
	
	/**
		Initialises a new successful response with the specified data.
		- Parameter data: The data from the response.
	*/
	public convenience init(data: DataType)
	{
		self.init(status: true, data: data)
	}
	
	public func hasError(forKey key: String) -> Bool
	{
		let errors = self.errors ?? [:]
		return errors.keys.contains(key)
	}
	
	public func error(forKey key: String) -> String?
	{
		let errors = self.errors ?? [:]
		return errors[key] ?? nil
	}
}
