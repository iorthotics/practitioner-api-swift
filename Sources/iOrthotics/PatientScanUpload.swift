import Foundation

// Represents the details for a patient scan upload.
public class PatientScanUpload
{
	// Specifies the URL for the file data to be read from.
	public let dataUrl: URL
	
	// Specifies the name of the file.
	public let filename: String
	
	/**
		Initialises a new patient scan upload.
		- Parameter dataUrl: The URL for the file data to be read from.
		- Parameter filename: Optional. The name of the file, otherwise the filename of the dataUrl parameter.
	*/
	public init(dataUrl: URL, _ filename: String? = nil)
	{
		self.dataUrl = dataUrl
		self.filename = filename ?? dataUrl.lastPathComponent
	}
}
