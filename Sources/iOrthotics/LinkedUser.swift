//
//  File.swift
//  
//
//  Created by Nick Bedford on 22/11/2023.
//

import Foundation

// Represents the information about a linked user.
public class LinkedUser : Codable
{
	public var guid: String = ""
	
	// Specifies the clinic name the order is destined for.
	public var clinicName: String = ""
	
	// Specifies the practitioner's first name.
	public var firstName: String = ""
	
	// Specifies the practitioner's last name
	public var lastName: String = ""
	
	public enum CodingKeys: String, CodingKey
	{
		case guid = "guid"
		case clinicName = "clinic_name"
		case firstName = "first_name"
		case lastName = "last_name"
	}
}
