import Foundation

// Represents the authentication information returned by the server.
public class AuthenticationInfo : Codable
{
	// Specifies the practitioner's GUID.
	public var guid: String? = nil
	
	// Specifies the practitioner's username (new field).
	public var username: String? = nil
	
	// Specifies the practitioner's first name.
	public var firstName: String
	
	// Specifies the practitioner's last name.
	public var lastName: String
	
	// Specifies the practitioner's API key.
	public var apiKey: String
	
	// Specifies the practitioner's clinic name.
	public var clinicName: String
	
	// Specifies whether patients must be created as an MRN not a first/last name.
	public var patientsRequireMRN: Bool? = nil
	
	public var showPatientDOB: Bool? = nil
	
	// Optionally specifies the expiry timestamp of the API key.
	public var apiKeyExpires: Int? = nil
	
	public var enterDobAsAge: Bool? = nil
	
	public enum CodingKeys: String, CodingKey
	{
		case guid = "guid"
		case username = "username"
		case firstName = "first_name"
		case lastName = "last_name"
		case apiKey = "api_key"
		case clinicName = "clinic_name"
		case patientsRequireMRN = "mrn_patients"
		case apiKeyExpires = "api_key_expires"
		case showPatientDOB = "show_patient_dob"
		case enterDobAsAge = "enter_dob_as_age"
	}
}
