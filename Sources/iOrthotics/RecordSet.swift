import Foundation

// Represents a record set API response.
public class RecordSet<RecordType: Decodable> : Decodable
{
	// Specifies the entries for this page of the record set.
	public var records: [RecordType] = []
	
	// Specifies the page information for the record set.
	public var page: PageInfo = PageInfo()
	
	public init()
	{
    }
	
	/**
		Initialises a record set with a list of records.
		- Parameter records: The records of the set.
	*/
	public convenience init(records: [RecordType])
	{
		self.init()
		self.records = records
		self.page.length = records.count
		self.page.total = self.page.length
	}
}
