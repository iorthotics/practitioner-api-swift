import Foundation

// Represents an order and its metadata.
public class Order : Codable
{
	// Specifies the order number.
	public var orderNumber: String = ""
	
	// Specifies the order date.
	public var orderDate: String = "1970-01-01 00:00:00"
	
	// Specifies the creation date of the order.
	public var createdAt: Either<Int, String>? = nil
	
	// Specifies the date the order record was updated.
	public var updatedAt: Either<Int, String>? = nil
	
	// Specifies the type of order, such as Foot_orthotic.
	public var type: String? = nil
	
	// Specifies variant type of the order, such as Semi-Custom or Design_Supplied.
	public var typeVariant: String? = nil
	
	// Specifies the podiatrist's first name.
	public var podiatristFirstName: String? = nil
	
	// Specifies the podiatrist's last name.
	public var podiatristLastName: String? = nil
	
	// Specifies the patient's GUID.
	public var patientGuid: String? = nil
	
	// Specifies the patient's first name.
	public var patientFirstName: String? = nil
	
	// Specifies the patient's last name.
	public var patientLastName: String? = nil
	
	// Specifies the patient's gender.
	public var patientGender: PatientGender? = nil
	
	// Specifies the patient's date of birth.
	public var patientDateOfBirth: String? = nil
	
	// Specifies the material being used to manufacture the order.
	public var material: String? = nil
	
	// Specifies the tracking number for the order.
	public var trackingNumber: String? = nil
	
	// Specifies the number of pairs being manufactured.
	public var pairCount: Int? = 1
	
	// Specifies whether the order is being express made.
	public var expressDelivery: Bool = false
	
	// Specifies the projected shipped by date.
	public var shippedByDate: String? = "1970-01-01"
	
	// Specifies whether the order requires design approval.
	public var approvalRequired: Bool = false
	
	// Specifies the status of the order.
	public var status: String = OrderStatus.orderReceived.string
	
	// Specifies the date the status was last changed.
	public var statusChangedAt: String? = nil
	
	// Specifies the Print_3d type order details.
	public var print3D: Print3D? = nil
	
	// Specifies the Foot_orthotic type order details.
	public var footOrthotic: FootOrthotic? = nil
	
	// Specifies the Print_mill type order details.
	public var printMill: PrintMillShell? = nil
	
	public enum CodingKeys: String, CodingKey
	{
		case orderNumber = "order_number"
		case orderDate = "order_date"
		case createdAt = "created_at"
		case updatedAt = "updated_at"
		case type = "order_type"
		case typeVariant = "order_type_variant"
		case podiatristFirstName = "podiatrist_first_name"
		case podiatristLastName = "podiatrist_last_name"
		case patientGuid = "patient_guid"
		case patientFirstName = "patient_first_name"
		case patientLastName = "patient_last_name"
		case patientDateOfBirth = "patient_birthdate"
		case patientGender = "patient_gender"
		case material
		case trackingNumber = "tracking_number"
		case pairCount = "pair_count"
		case expressDelivery = "express"
		case shippedByDate = "shipped_by"
		case approvalRequired = "approval_required"
		case status
		case statusChangedAt = "statuschanged_at"
		case print3D = "Print_3d"
		case footOrthotic = "Foot_orthotic"
		case printMill = "Print_mill"
	}
}
