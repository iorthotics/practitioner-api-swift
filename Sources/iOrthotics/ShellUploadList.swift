// Represents a list of shells to upload.
public class ShellUploadList : Codable
{
	// Specifies the list of shells to upload.
	public let shells: [ShellUpload]
	
	/**
		Initialises a list of shell uploads.
		- Parameter shells: The list of shells.
	*/
	public init(shells: [ShellUpload])
	{
		self.shells = shells
	}
}
