import Foundation

extension Dictionary where Key == String, Value == String?
{
	// Formats the dictionary as a URL encoded query string with a preceding question mark.
	var urlEncodedQueryString: String
	{
		let params = self.filter { (key: String, value: String?) -> Bool in
			return value != nil
		} .map { (key: String, value: String?) -> String in
			return "\(key)=\(value!.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)"
		}
		return "?\(params.joined(separator: "&"))"
	}
	
	// Forms the dictionary as a POST request content body in the form of application/x-www-form-urlencoded data.
	var formUrlEncodedContent: Data?
	{
		let encoded = self.urlEncodedQueryString
		let content = encoded.count > 1 ? encoded.suffix(encoded.count - 1) : ""
		return content.data(using: String.Encoding.ascii)
	}
}
