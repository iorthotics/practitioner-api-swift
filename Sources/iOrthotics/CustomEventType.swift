//
//  File.swift
//  
//
//  Created by Nick Bedford on 12/12/2023.
//

import Foundation

public enum CustomEventType: String
{
	case SnugFitScanFO = "SnugFitScanFO"
	case SnugFitScanAFO = "SnugFitScanAFO"
	case StructureScanFO = "StructureScanFO"
	case StructureScanAFO = "StructureScanAFO"
	case StructureScanOther = "StructureScanOther"
	case StructureScanFBox = "StructureScanFBox"
}
