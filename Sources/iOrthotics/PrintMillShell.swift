import Foundation

// Represents a Print Mill/Shell order.
public class PrintMillShell : Codable
{
	// Specifies the type of order.
	public var type: String? = nil
	
	// Specifies the shells being manufactured.
	public var shells: [PrintMillShellJob]? = []
}
