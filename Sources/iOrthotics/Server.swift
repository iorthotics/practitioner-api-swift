import Foundation

// Represents the information about an API server.
public class Server
{
	// Specifies the friendly name of the server.
	public let name: String
	
	// Specifies the ID of the server, such as "aus" or "usa".
	public let serverId: String
	
	// Specifies the base URL of the API.
	public let baseUrl: String
	
	// Specifies the default headers to add to every request.
	public var defaultHeaders: [String: String] = [:]
	
	// Gets or sets the URL session to use for requests.
	public var urlSession: URLSession? = nil
	
	// Specifies whether date formatting should use a US style m/d/y order.
	public let USDateFormat: Bool
	
	// Gets the minimum required configuration.
	public static var minimumUrlSessionConfiguration: URLSessionConfiguration
	{
		get
		{
			let configuration = URLSession.shared.configuration.copy() as? URLSessionConfiguration ?? URLSessionConfiguration.default
			configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
			configuration.waitsForConnectivity = true
			configuration.httpAdditionalHeaders = [
				"Expect": "100-Continue"
			]
			
			if #available(iOS 13.0, *)
			{
				configuration.tlsMinimumSupportedProtocolVersion = .TLSv12
				configuration.tlsMaximumSupportedProtocolVersion = .TLSv13
			}
			else
			{
				configuration.tlsMinimumSupportedProtocol = .tlsProtocol11
				configuration.tlsMaximumSupportedProtocol = .tlsProtocol13
			}
			return configuration
		}
	}
	
	// Gets the default URL session.
	public static var defaultUrlSession: URLSession {
		return URLSession(configuration: minimumUrlSessionConfiguration)
	}
	
	/**
		Initialises a new server.
		- Parameter name: The name of the server.
		- Parameter serverId: The ID of the server.
		- Parameter baseUri: The base URL of the API.
	*/
	public init(name: String, serverId: String, baseUri: String, USDateFormat: Bool = false)
	{
		self.name = name
		self.serverId = serverId
		self.baseUrl = baseUri
		self.USDateFormat = USDateFormat
	}
	
	/**
		Initialises a copy of an existing server.
		- Parameter server: An existing server to copy.
	*/
	public convenience init(server: Server) {
		self.init(name: server.name,
				  serverId: server.serverId,
				  baseUri: server.baseUrl,
				  USDateFormat: server.USDateFormat)
		defaultHeaders = server.defaultHeaders
	}
	
	/**
		Creates a URL request for the server.
		- Parameter path: The API endpoint path excluding query parameters.
		- Parameter method: Optional. The HTTP method, otherwise "GET".
		- Parameter parameters: Optional. The query parameters to add to the request URL.
		- Parameter content: Optional. The request body content.
	*/
	func createRequest<ResponseType>(responseType: ResponseType.Type,
									 path: String,
									 method: String = "GET",
									 query: [String: String?]? = nil,
									 content: Data? = nil,
									 fallbackErrorHandler: ((Response<Int>) -> Void)? = nil,
									 headers: [String:String] = [:]) -> Request<ResponseType>
	{
		let urlPath = path +
			(query != nil && query!.count > 0 ? query!.urlEncodedQueryString : "")
		let url: URL = URL(string: self.baseUrl + urlPath)!
		
		var request = URLRequest(url: url)
		request.httpMethod = method
		request.allowsCellularAccess = true
		request.httpBody = content
		
		for header in defaultHeaders
		{
			request.addValue(header.value, forHTTPHeaderField: header.key)
		}
		
		for header in headers
		{
			let _ = request.addValue(header.value, forHTTPHeaderField: header.key)
		}
		
		return Request<ResponseType>(request: request, session: urlSession, fallbackErrorHandler: fallbackErrorHandler, originalPath: path)
	}
}
