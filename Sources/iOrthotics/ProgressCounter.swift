//
//  File.swift
//  
//
//  Created by Nick Bedford on 20/12/2023.
//

import Foundation

public class ProgressCounter
{
	public let complete: Int
	public let total: Int
	
	public init(complete: Int, total: Int)
	{
		self.complete = complete
		self.total = total
	}
	
	public var advanced: ProgressCounter
	{
		get
		{
			ProgressCounter(complete: complete + 1, total: total)
		}
	}
	
	public var isComplete: Bool
	{
		get
		{
			complete >= total
		}
	}
	
	public var progress: Float
	{
		get
		{
			Float(complete) / Float(total)
		}
	}
}
