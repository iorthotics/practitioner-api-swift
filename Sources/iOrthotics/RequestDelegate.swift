//
//  File.swift
//  
//
//  Created by Nick Bedford on 12/9/2023.
//

import Foundation

public protocol RequestDelegate
{
	func requestShouldContinue(to request: RequestBase, method: String, via api: PractitionerAPI, isAuthenticationRequest: Bool, async: @escaping () -> Void) -> Bool
}
