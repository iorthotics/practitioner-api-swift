//
//  File.swift
//  
//
//  Created by Nick Bedford on 14/12/2023.
//

import Foundation

public protocol ClientApplicationDelegate
{
	func mainClientApplication(wasReplacedWith: ClientApplication?)
	func clientApplication(apiChangedFor: ClientApplication)
}
