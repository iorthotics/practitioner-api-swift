import Foundation

// Represents a shell order upload.
public class ShellUpload : Codable
{
	// Specifies the file key. This will be overwritten by the API.
	public var fileKey: String = ""
	
	// Specifies the engraving text for the shells.
	public var engravingText: String = ""
	
	// Specifies the reference nubmer for the shells.
	public var referenceNumber: String = ""
	
	// Specifies the number of pairs to order.
	public var pairs: Int = 1
	
	// Specifies the EVA density for an EVA Shell order.
	public var evaDensity: EVADensity? = nil
	
	// Specifies the URL of the file to upload.
	public var fileUrl: URL? = nil
	
	/**
		Initialises a new shell upload.
		- Parameter fileUrl: The URL of the file to upload.
		- Parameter pairs: The number of pairs to order.
		- Parameter engravingText: The engraving text for the shells.
		- Parameter referenceNumber: The reference nubmer for the shells.
		- Parameter evaDensity: The EVA density for an EVA Shell order.
	*/
	public init(fileUrl: URL, pairs: Int = 1, engravingText: String = "", referenceNumber: String = "", evaDensity: EVADensity? = nil)
	{
		self.engravingText = engravingText
		self.referenceNumber = referenceNumber
		self.evaDensity = evaDensity
		self.pairs = pairs
		self.fileUrl = fileUrl
	}
	
	public enum CodingKeys: String, CodingKey
	{
		case fileKey = "file"
		case engravingText
		case referenceNumber
		case pairs
		case evaDensity
	}
}
