//
//  DateTimeStamp.swift
//  
//
//  Created by Nick Bedford on 2/8/2022.
//

import Foundation

public enum Either<A, B>: Codable where A: Codable, B: Codable
{
	case a(A)
	case b(B)
	
	public init(from decoder: Decoder) throws
	{
		let container = try decoder.singleValueContainer()
		do
		{
			self = try .a(container.decode(A.self))
		}
		catch DecodingError.typeMismatch
		{
			do
			{
				self = try .b(container.decode(B.self))
			}
			catch DecodingError.typeMismatch
			{
				throw DecodingError.typeMismatch(Either<A, B>.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Encoded payload not of an expected type."))
			}
		}
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.singleValueContainer()
		switch self {
			case .a(let a):
				try container.encode(a)
			
			case .b(let b):
				try container.encode(b)
		}
	}
}
