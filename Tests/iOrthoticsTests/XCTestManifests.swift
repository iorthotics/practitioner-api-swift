import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
		testCase(JsonTests.allTests),
		testCase(HttpDataTests.allTests),
		testCase(ApiTests.allTests)
    ]
}
#endif
