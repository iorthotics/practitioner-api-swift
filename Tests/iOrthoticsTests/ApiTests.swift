import XCTest
import Foundation
@testable import iOrthotics

final class ApiTests: XCTestCase, RequestDelegate
{
	func requestShouldContinue(to request: RequestBase, method: String, via api: iOrthotics.PractitionerAPI, isAuthenticationRequest: Bool, async: @escaping () -> Void) -> Bool {
		
		DispatchQueue.main.async {
			print("==> Executing \(method) request to \(request.url!.absoluteString)...")
			async()
		}
		return false
	}
	
	private var api: PractitionerAPI? = nil
	private var expectation: XCTestExpectation? = nil
	
	override func setUp() {
		api = PractitionerAPI(server: PractitionerAPI.developmentServer,
							  apiKey: "ol78ZjvP93EezIvZVNxQWHfzpwMep64e")
		api?.delegate = self
		expectation = expectation(description: "Complete")
	}
	
	func testCreateCustomEvent() {
		api = PractitionerAPI(server: PractitionerAPI.developmentServer,
							  apiKey: "ol78ZjvP93EezIvZVNxQWHfzpwMep64e")
		let _ = api?.createCustomEvent(type: .SnugFitScanFO,
									   description: "FO Scan for J. Smith",
									   data: [
										"Hello": "world!"
									   ]) { response in
			XCTAssertTrue(response.status, response.reason ?? "Unknown")
			self.expectation?.fulfill()
		}
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testCheckOkay() {
		api = PractitionerAPI(server: PractitionerAPI.stagingServer,
							  apiKey: "123")
		let _ = api?.checkOkay { response in
			XCTAssertTrue(response.status)
			XCTAssertNotNil(response.data)
			XCTAssertEqual("Australia/Brisbane", response.data!["timezone"])
			self.expectation?.fulfill()
		}
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testGetApiKey() {
		api?.fallbackErrorHandler = { response in
			print(response.reason!)
			self.expectation?.fulfill()
		}
		let _ = api?.authenticatePractitioner(username: "api.test.user",
										   password: "XTLkNnmQVC6iVkJA2s9N!",
										   applicationKey: "dfc9e43254017ae92e0afe6b6750d57a1d1f9898",
										   completion: { (response: Response<AuthenticationInfo>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			XCTAssertEqual(response.data?.firstName ?? nil, "API Test")
			XCTAssertEqual(response.data?.lastName ?? nil, "User")
			XCTAssertTrue((response.data?.apiKey ?? "").count > 0)
			self.expectation?.fulfill()
		})
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testGetApiKeyWithExpiringKeyThenRefresh() {
		api?.fallbackErrorHandler = { response in
			print(response.reason!)
			self.expectation?.fulfill()
		}
		let uuid = UUID().uuidString
		let _ = api?.authenticatePractitioner(username: "api.test.user",
										   password: "XTLkNnmQVC6iVkJA2s9N!",
										   applicationKey: "dfc9e43254017ae92e0afe6b6750d57a1d1f9898",
										   deviceGuid: uuid,
										   completion: { (response: Response<AuthenticationInfo>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			XCTAssertEqual(response.data?.firstName ?? nil, "API Test")
			XCTAssertEqual(response.data?.lastName ?? nil, "User")
			let expiresAfter = Int(Date.timeIntervalSinceReferenceDate + Date.timeIntervalBetween1970AndReferenceDate) + (3600 * 11)
			XCTAssertTrue(response.data!.apiKeyExpires! > expiresAfter)
			XCTAssertTrue((response.data?.apiKey ?? "").count > 0)
			
			let originalApiKey = response.data!.apiKey
			self.api?.apiKey = originalApiKey
			self.api?.apiKeyExpires = response.data!.apiKeyExpires
			let expires = response.data!.apiKeyExpires!
			XCTAssertGreaterThan(self.api!.secondsUntilApiKeyExpires!, TimeInterval(3600 * 11))
			
			DispatchQueue.main.asyncAfter(deadline: .now().advanced(by: .seconds(2))) {
				
				self.api?.refreshTemporaryApiKey(deviceGuid: uuid) { response in
					
					XCTAssertTrue(response.status)
					XCTAssertGreaterThan(response.data!.apiKeyExpires, expires)
					XCTAssertNotEqual(self.api!.apiKey, originalApiKey)
					
					self.api?.revokeTemporaryApiKey { response in
						
						XCTAssertTrue(response.status)
						
						self.api?.refreshTemporaryApiKey(deviceGuid: uuid) { response in
							
							XCTAssertFalse(response.status)
							XCTAssertEqual("Invalid authentication.", response.reason!)
							self.expectation?.fulfill()
						}
					}
				}
			}
		})
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testCreateSessionUrl() {
		let url = "https://staging.iorthotics.com.au/practitioner/practice"
		let _ = api?.createSessionUrl(url: url, completion: { (response: Response<SessionUrl>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			let sessionUrl = String(response.data?.url ?? "")
			XCTAssertTrue(sessionUrl.hasPrefix(url))
			XCTAssertTrue(sessionUrl.contains("?authtoken="))
		 	self.expectation?.fulfill()
	 	})
	 
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testGetGenders() {
		let _ = api?.getGenders { response in
			XCTAssertTrue(response.status)
			XCTAssertNotNil(response.data)
			XCTAssertGreaterThanOrEqual(response.data?.count ?? 0, 2)
			self.expectation?.fulfill()
		}
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
    func testGetOrders() {
		let _ = api?.getOrders(query: OrdersQuery(page: 0, orderNumber: "100009"), completion: { (response: Response<RecordSet<Order>>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			let order = response.data?.records.first
			XCTAssertNotNil(order)
			XCTAssertEqual(order!.type ?? nil, "Foot_orthotic")
			XCTAssertEqual(order!.status, OrderStatus.shipped.string)
			XCTAssertFalse(order!.expressDelivery)
			self.expectation?.fulfill()
		})
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testRecordTechMed3D() {
		 let _ = api?.recordTechMed3DScan(patientGuid: "aee3d7f2-f063-11ed-9cc5-d2be3f034f83", completion: { (response: Response<Int>) in
			 XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			 self.expectation?.fulfill()
		 })
		 
		 waitForExpectations(timeout: 30, handler: nil)
	 }
	
	func testRecordTechMed3DAFO() {
		 let _ = api?.recordTechMed3DScan(patientGuid: "aee3d7f2-f063-11ed-9cc5-d2be3f034f83", quantity: 2, completion: { (response: Response<Int>) in
			 XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			 self.expectation?.fulfill()
		 }, type: .afo)
		 
		 waitForExpectations(timeout: 30, handler: nil)
	 }
	
    func testGetPatients() {
		let _ = api?.getPatients(query: PatientsQuery(page: 0, search: "Bonnie Prosacco"), completion: { (response: Response<RecordSet<Patient>>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			XCTAssertEqual(response.data?.records[0].firstName.trimmingCharacters(in: .whitespacesAndNewlines) ?? nil, "Bonnie")
			XCTAssertEqual(response.data?.records[0].dateOfBirth ?? nil, "1980-09-08")
			XCTAssertEqual(response.data?.records[0].gender, PatientGender.female)
			self.expectation?.fulfill()
		})
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
    func testCreateAndEditPatient() {
		let patient = Patient(firstName: "John", lastName: "Citizen", dateOfBirth: "1987-01-02".toDate()!, height: 189, weight: 89, gender: .male, externalID: "EXTERNAL")
		
		let _ = api?.createPatient(patient: patient, completion: { (response: Response<RecordSet<Patient>>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			let created = response.data?.records[0]
			XCTAssertEqual(created?.firstName ?? nil, "MYF001")
			XCTAssertEqual(created?.lastName ?? nil, "Citizen")
			XCTAssertEqual(created?.dateOfBirth ?? nil, "1987-01-02")
			XCTAssertEqual(created?.height, 189)
			XCTAssertEqual(created?.weight, 89)
			XCTAssertEqual(created?.externalID, "Citizen")
			XCTAssertEqual(created?.gender, PatientGender.male)
			
			self.editPatientTest(patient: created!)
		})

		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func editPatientTest(patient: Patient) {
		patient.firstName = "Janet"
		patient.lastName = "Goodall"
		patient.dateOfBirth = "1930-03-02"
		patient.gender = .female
		
		let _ = api?.updatePatient(patient: patient, completion: { (response: Response<RecordSet<Patient>>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			let updated = response.data?.records[0]
			XCTAssertEqual(updated?.firstName ?? nil, patient.firstName)
			XCTAssertEqual(updated?.lastName ?? nil, patient.lastName)
			XCTAssertEqual(updated?.dateOfBirth ?? nil, patient.dateOfBirth)
			XCTAssertEqual(updated?.height, 189)
			XCTAssertEqual(updated?.weight, 89)
			XCTAssertEqual(updated?.gender, PatientGender.female)
			
			self.expectation?.fulfill()
		})
	}
	
    func testUploadAndDeletePatientScans()
	{
		let text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
		
		let binary = Data((0...100).map { (_: Int) -> UInt8 in
			UInt8.random(in: 0...255)
			})

		let file0 = URL(fileURLWithPath: "test_file0.txt")
		let file1 = URL(fileURLWithPath: "test_file1.dat")
		
		do {
			try text.data(using: .utf8)?.write(to: file0)
			try binary.write(to: file1)
		}
		catch {
			XCTFail(error.localizedDescription)
		}
		
		let files = [
			PatientScanUpload(dataUrl: file0),
			PatientScanUpload(dataUrl: file1)
		]
		
		let patientGuid = "b5338e0c-97fb-11eb-8923-02207db7d13e"
		api?.upload(scans: files,
					patientGuid: patientGuid,
					scanGroupGuid: UUID(),
					completion: { (response: Response<RecordSet<PatientScan>>) in
				XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
				XCTAssertEqual(response.data?.records.count ?? -1, 2)
				XCTAssertEqual(response.data?.records[0].filename ?? nil, file0.lastPathComponent)
				
				self.deletePatientScan(scan: response.data?.records[0] ?? nil, patientGuid: patientGuid)
		})
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func deletePatientScan(scan: PatientScan?, patientGuid: String) {
		api?.deletePatientScan(patientGuid: patientGuid, scanGuid: scan?.guid! ?? "nil", completion: { (response: Response<Int>) in
			XCTAssertTrue(response.status)
			self.expectation?.fulfill()
		})
	}
	
	func testCreate3dShellOrder() {
		let text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
		
		let file0 = URL(fileURLWithPath: "test_file0.stl")
		let file1 = URL(fileURLWithPath: "test_file1.stl")
		
		do {
			try text.data(using: .utf8)?.write(to: file0)
			try text.data(using: .utf8)?.write(to: file1)
		}
		catch {
			XCTFail(error.localizedDescription)
		}
		
		let shells = [
			ShellUpload(fileUrl: file0, pairs: 1, engravingText: "HELLO", referenceNumber: "12345"),
			ShellUpload(fileUrl: file1, pairs: 2, engravingText: "HELLO", referenceNumber: "54321")
		]
		
		api?.create3DShellOrder(shells: shells, completion: { (response: Response<OrdersCreated>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			XCTAssertEqual(response.data?.orderNumbers.count ?? -1, 3)
			self.expectation?.fulfill()
		})
		
		waitForExpectations(timeout: 30, handler: nil)
	}
	
	func testCreateEvaShellOrder() {
		let text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
		
		let file0 = URL(fileURLWithPath: "test_file0.stl")
		let file1 = URL(fileURLWithPath: "test_file1.stl")
		
		do {
			try text.data(using: .utf8)?.write(to: file0)
			try text.data(using: .utf8)?.write(to: file1)
		}
		catch {
			XCTFail(error.localizedDescription)
		}
		
		let shells = [
			ShellUpload(fileUrl: file0, pairs: 3, engravingText: "HELLO", referenceNumber: "12345", evaDensity: .Medium),
			ShellUpload(fileUrl: file1, pairs: 1, engravingText: "HELLO", referenceNumber: "54321", evaDensity: .Soft)
		]
		
		api?.createEVAShellOrder(shells: shells, completion: { (response: Response<OrdersCreated>) in
			XCTAssertTrue(response.status, response.reason ?? "Unknown error.")
			XCTAssertEqual(response.data?.orderNumbers.count ?? -1, 4)
			self.expectation?.fulfill()
		})
		
		waitForExpectations(timeout: 30, handler: nil)
	}

    static var allTests = [
        ("testGetPatients", testGetPatients),
		("testCreateSessionUrl", testCreateSessionUrl),
		("testGetOrders", testGetOrders),
		("testCreateAndEditPatient", testCreateAndEditPatient),
		("testUploadAndDeletePatientScans", testUploadAndDeletePatientScans),
		("testCreate3dShellOrder", testCreate3dShellOrder)
    ]
}
