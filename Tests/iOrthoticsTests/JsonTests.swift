import XCTest
@testable import iOrthotics

final class JsonTests: XCTestCase
{
    func testRecordSetDeserialize()
	{
		let json = "{\"status\":true,\"data\":{\"records\":[1,2,3],\"page\":{\"page\":0,\"previous\":null,\"next\":1,\"length\":100,\"total\":123}}}"
		
		let response = json.decodeJson(Response<RecordSet<Int>>.self)
		
		XCTAssertNotNil(response)
		XCTAssertTrue(response?.status ?? false)
		XCTAssertEqual(response?.data?.records[1] ?? -1, 2)
		XCTAssertEqual(response?.data?.records.count ?? -1, 3)
    }
	
    func testFailedResponseDeserialize()
	{
		let json = "{\"status\":false,\"reason\":\"Request failed\"}"
		
		let response = json.decodeJson(Response<Int>.self)
		
		XCTAssertNotNil(response)
		XCTAssertFalse(response?.status ?? true)
		XCTAssertEqual(response?.reason ?? nil, "Request failed")
    }

    static var allTests = [
        ("testRecordSetDeserialize", testRecordSetDeserialize),
        ("testFailedResponseDeserialize", testFailedResponseDeserialize),
    ]
}
