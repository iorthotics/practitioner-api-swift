import XCTest
@testable import iOrthotics

final class DataTests: XCTestCase
{
    func testMultipartFormDataAttachment()
	{
		let text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
		let file = text.data(using: .utf8)
		let fileUrl = URL(fileURLWithPath: "SomeFile.txt")
		do {
			try file?.write(to: fileUrl)
		}
		catch {
			XCTFail(error.localizedDescription)
		}
		
		let formData = MultipartFormData()
		do {
			try formData.addFile(url: fileUrl)
			formData.addValue("Some Value", withName: "some_value")
		}
		catch {
			XCTFail(error.localizedDescription)
		}
		
        let data = formData.finalise()
		let lines = String(data: data, encoding: .utf8)?.split { $0.isNewline }.map { String($0) }
		XCTAssertNotNil(formData)
		XCTAssertNotNil(lines)
		
		XCTAssertEqual(String(lines![0]),
                       "--\(formData.boundary)")
		XCTAssertTrue(lines!.contains("Content-Disposition: attachment; name=\"file0\"; filename=\"SomeFile.txt\""))
		XCTAssertTrue(lines!.contains("Content-Type: text/plain"))
		XCTAssertTrue(lines!.contains(text))
    }

    static var allTests = [
        ("testMultipartFormDataAttachment", testMultipartFormDataAttachment)
    ]
}
