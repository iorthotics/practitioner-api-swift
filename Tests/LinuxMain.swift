import XCTest

import iOrthoticsTests

var tests = [XCTestCaseEntry]()
tests += iOrthoticsTests.allTests()
XCTMain(tests)
